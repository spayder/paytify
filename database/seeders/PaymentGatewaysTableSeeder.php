<?php

namespace Database\Seeders;

use App\Models\PaymentGateway;
use Illuminate\Database\Seeder;

class PaymentGatewaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentGateway::create([
            'name' => 'PayPal',
            'image' => 'img/payment-gateways/paypal.jpg'
        ]);

        PaymentGateway::create([
            'name' => 'Stripe',
            'image' => 'img/payment-gateways/stripe.jpg'
        ]);
    }
}
